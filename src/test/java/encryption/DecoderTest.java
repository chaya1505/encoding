package encryption;

import org.junit.Test;

import java.net.URL;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DecoderTest {

    Decoder decoder = new Decoder();

    @Test
    public void decode_correctEncodedFile_success(){
        byte[] h0 = Utils.hexStringToByteArray("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f");
        URL url = Thread.currentThread().getContextClassLoader().getResource("encodedFile.bin");
        boolean res = decoder.decode(url.getPath(), h0 );
        assertTrue(res);
    }

    @Test(expected = IllegalArgumentException.class)
    public void decode_incorrectH0_shouldGetIllegalArgumentException(){
        byte[] h0 = Utils.hexStringToByteArray("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e777");
        URL url = Thread.currentThread().getContextClassLoader().getResource("encodedFile.bin");
        decoder.decode(url.getPath(), h0 );
    }

    @Test(expected = IllegalArgumentException.class)
    public void decode_incorrectEncodedFile_shouldGetIllegalArgumentException(){
        byte[] h0 = Utils.hexStringToByteArray("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f");
        URL url = Thread.currentThread().getContextClassLoader().getResource("zero.bin");
        decoder.decode(url.getPath(), h0 );
    }

    @Test
    public void decode_fileDoesNotExist_returnFalse(){
        byte[] h0 = Utils.hexStringToByteArray("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f");
        boolean res = decoder.decode("fileDoesNotExist.bin", h0 );
        assertFalse(res);
    }

}